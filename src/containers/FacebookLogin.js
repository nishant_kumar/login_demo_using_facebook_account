import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Facebook from "components/login/Facebook";
import actions from "actions";

class FacebookLogin extends Component {
    render () {
        const { actions, history } = this.props;
        return (
            <>
                <Facebook actions={ actions } history={ history } />
            </>
        );
    }
}

function mapStateToProps (state) {
    return { generic: state.generic };
}

function mapDispatchToProps (dispatch) {
    const actionMap = { actions: bindActionCreators(actions, dispatch) };
    return actionMap;
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FacebookLogin);
