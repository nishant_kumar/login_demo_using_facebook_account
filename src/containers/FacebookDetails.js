import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FacebookProfile from "components/generic/FacebookProfile";
import actions from "actions";

class FacebookDetails extends Component {
  render() {
    const { generic, actions, history, match } = this.props;
    return (
      <>
        <FacebookProfile
          generic={generic}
          history={history}
          actions={actions}
          match={match}
        />
      </>
    );
  }
}

function mapStateToProps(state) {
  return { generic: state.generic };
}

function mapDispatchToProps(dispatch) {
  const actionMap = { actions: bindActionCreators(actions, dispatch) };
  return actionMap;
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FacebookDetails);
