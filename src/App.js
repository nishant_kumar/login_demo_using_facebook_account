import React from "react";
import Routes from "./Routes";
import "./app.scss";

const App = () => {
  return (
    <div className="bootstrap-wrapper">
      <div className="home-container">
        <Routes />
      </div>
    </div>
  );
};

export default App;
