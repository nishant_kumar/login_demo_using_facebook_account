import {
  GET_FACEBOOK_DETAILS
} from "actions/const";

function reducer(state = null, action) {
  const nextState = Object.assign({}, state);
  switch (action.type) {
    case GET_FACEBOOK_DETAILS:
      nextState.facebookDetailsData = action.payload;
      return nextState;
    default:
  }
  return state;
}

export default reducer;
