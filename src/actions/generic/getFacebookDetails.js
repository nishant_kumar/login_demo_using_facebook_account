import { GET_FACEBOOK_DETAILS } from "actions/const";
import { GetFacebookDetails } from "services/generic";

function action (accessToken) {
  return dispatch => {
    GetFacebookDetails(accessToken)
      .then(function(response) {
        dispatch({ type: GET_FACEBOOK_DETAILS, payload: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  };
}
export default action;
