//generic
import getFacebookDetails from "actions/generic/getFacebookDetails";

const actions = {
  getFacebookDetails
};

export default actions;
