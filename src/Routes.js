import React, { Component } from "react";
import { Route, Switch} from "react-router-dom";
import FacebookLogin from "./containers/FacebookLogin";
import FacebookDetails from "./containers/FacebookDetails";

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact={ true } path={`/`} component={ FacebookLogin} />
        <Route exact={ true } path={ `/profile-details/:token` } component={ FacebookDetails } />
      </Switch>
    );
  }
}

export default Routes;
