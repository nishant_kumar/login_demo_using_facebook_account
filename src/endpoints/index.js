const url = "https://graph.facebook.com";
const locale = "/me";
const version = "/v3.3";
const baseUrl = url + version + locale;
export {baseUrl}
