import React, { Component } from "react";
import "./facebook-profile.scss";
import folderImage from "images/folder.jpg";

class FacebookProfile extends Component {
    constructor () {
        super();
        this.state = {
            albumImages: [],
            albumImagesIndex: 0,
            albumName: '',
            isShowAlbumPopup: false
        }
        this.getAlbumData = this.getAlbumData.bind(this);
        this.previousAlbumImage = this.previousAlbumImage.bind(this);
        this.nextAlbumImage = this.nextAlbumImage.bind(this);
        this.closeAlbumPopup = this.closeAlbumPopup.bind(this);
    }

    componentDidMount () {
        const { actions, match: { params } } = this.props;
        const accessToken = params.token ? params.token : null;
        accessToken && actions.getFacebookDetails(accessToken);
    }

    getAlbumData (album) {
        if (album.photo_count > 0) {
            let images = [];
            let data = album.photos.data;
            for (let index = 0; index < data.length; index++) {
                images.push(data[index].webp_images[0].source);
            }
            this.setState({ albumImages: images});
            this.setState({ albumName: album.name});
            this.setState({ isShowAlbumPopup: true});
        }
    }

    previousAlbumImage (currentImgIndex) {
        const { albumImages } = this.state;
        if (albumImages[currentImgIndex - 1]) {
            this.setState({ albumImagesIndex: currentImgIndex - 1});
        }
    }

    nextAlbumImage (currentImgIndex) {
        const { albumImages } = this.state;
        if (albumImages[currentImgIndex + 1]) {
            this.setState({ albumImagesIndex: currentImgIndex + 1 });
        }
    }

    closeAlbumPopup () {
        this.setState({ isShowAlbumPopup: false });
        this.setState({ albumImagesIndex: 0 });
    }

    render () {
        const { generic } = this.props;
        const { albumImages, albumImagesIndex, albumName, isShowAlbumPopup } = this.state;

        const profileDetails = generic ? 
            generic.facebookDetailsData ?
                generic.facebookDetailsData
                : null
            : null;

        const images = profileDetails ?
            profileDetails.albums ?
                profileDetails.albums.data
                : null
            : null;

        let profilePictures = '';
        let albums = [];
        if (images) {
            for (let index = 0; index < images.length; index++){
                if (images[index].type === "profile"){
                    profilePictures = images[index].photos.data[0].webp_images[0].source
                }

                if (images[index].type === "normal") {
                    albums.push(images[index]);
                }
            }
        }

        return (
            <>
                { profileDetails && <div className="sub-header">Here is your profile details</div>}
                { profileDetails && 
                    <div className="profile">
                        <div className="profile-details">
                            <div>Name : { profileDetails.name }</div>
                            <div>ID no. : { profileDetails.id }</div>
                            <div>Email : { profileDetails.email }</div>
                            <div>gender : { profileDetails.gender }</div>
                        </div>
                        <div className="profile-image">
                            { profilePictures && <img
                                src={ profilePictures }
                                alt='Profile pic'
                            />}
                        </div>
                    </div>
                }

                { albums && <div className="sub-header">Here is your albums</div> }

                { albums && <div className="albums">
                    { albums &&
                        albums.map((obj, i) => (
                            <div key={ i } className="albums-folder">
                                <div className="folder" onClick={ () => this.getAlbumData(obj) }>
                                    <img src={ folderImage } alt="Album folder" />
                                    <div>{ obj.name }</div>
                                </div>
                            </div>
                        )) }
                </div>}

                { isShowAlbumPopup && albumImages && albumImages.length > 0 &&
                    <div className="albums-container">
                        <div className="album-header">Here is { albumName } images
                            <span onClick={ this.closeAlbumPopup }>Close</span>
                        </div>
                        <div className="albums-images">
                        { albumImagesIndex === 0 && <div className="previous not-allowed"><span>Prev</span></div> }
                            { albumImagesIndex > 0 && <div className="previous">
                                <span onClick={ () => this.previousAlbumImage(albumImagesIndex) }>Prev</span>
                            </div>}
                            <img src={ albumImages[albumImagesIndex] } alt="Album pic" />
                            { albumImagesIndex < albumImages.length - 1 && <div className="next">
                                <span onClick={ () => this.nextAlbumImage(albumImagesIndex) }>Next</span>
                            </div>}
                        { albumImagesIndex === albumImages.length - 1 && <div className="next not-allowed"><span>Next</span></div> }
                        </div>
                    </div>
                }
            </>
        );
    }
}

export default FacebookProfile;
