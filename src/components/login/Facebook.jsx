import React, { Component } from "react";
import FacebookLogin from "react-facebook-login";
import { appId } from "configuration";

class Facebook extends Component {
    constructor () {
        super();
        this.state = {
            isLoggedIn: false,
            userID: "",
            name: "",
            email: "",
            picture: ""
        }
    }

    responseFacebook = respose => {
        respose.accessToken && this.setState({
            isLoggedIn: true,
            userID: respose.userID,
            name: respose.name,
            email: respose.email,
            picture: respose.picture && respose.picture.data.url
        });

        const { history } = this.props;
        if (respose.accessToken) {
            const path = {
                pathname: `/profile-details/${ respose.accessToken }`,
                accessToken: respose.accessToken && respose.accessToken,
                title: 'Nishant test'
            };
            history.push(path);
        }
    }

    render () {
        let fbContent;
        if(this.state.isLoggedIn){
            fbContent = (
                <div
                style={{
                    width: '500px',
                    margin: 'auto',
                    background: '#f4f4f4',
                    padding: '20px'
                }}
                >
                    <img src={this.state.picture} alt={this.state.name} />
                    <h2>Welcome {this.state.name}</h2>
                    Email: {this.state.email}
                </div>
            );
        } else {
            fbContent = (
                <FacebookLogin
                    appId={ appId }
                    autoLoad={ true }
                    fields="name,email,picture"
                    callback={ this.responseFacebook } />
            );
        }

        return (
            <>
                <div>{ fbContent }</div>
            </>
        );
    }
}

export default Facebook;
