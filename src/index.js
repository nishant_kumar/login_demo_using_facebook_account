import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import reducers from 'reducers'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import HttpsRedirect from 'react-https-redirect'
import { createStore, applyMiddleware, compose } from 'redux'
import App from './App'
import * as serviceWorker from './serviceWorker'

const composeEnhancers = compose
const middleware = composeEnhancers(applyMiddleware(thunk))
const store = createStore(reducers, middleware)

ReactDOM.render(
    <Provider store={ store }>
        <Router>
            <HttpsRedirect>
                <App />
            </HttpsRedirect>
        </Router>
    </Provider>,
    document.getElementById('root'),
)

serviceWorker.register()
