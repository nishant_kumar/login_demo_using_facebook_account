import axios from "axios";
import { baseUrl } from "endpoints";

function GetFacebookDetails (accessToken) {
  return axios.get(baseUrl + `?fields=id,name,albums{name,type,photo_count,photos{webp_images}},gender,email&access_token=${accessToken}`);
}

export {
  GetFacebookDetails
};
